var BINARIUM = {

    elements: {},
    actions: {},
    helpers: {},

    start: function () {
        this.$window = $(window);
        this.$document = $(document);

        var self = this;
        this.$document.ready(function(){
            self._DOMReadyHandler();
        });
    },

    _DOMReadyHandler: function () {
        this.elements.Tabs.init();
        this.elements.Dropdown.init();
        this.elements.Tooltip.init();
        this.elements.GMTSession.init();
        this.elements.ScrollReveal.init();
        this.elements.Modal.init();
    }
}

BINARIUM.elements.Modal = {
    init: function () {

        $('.modal').on('show.bs.modal', function () {
            var _this = $(this);

            if (_this.data('source')) {
                _this.find('iframe').attr('src', _this.data('source'));
            }
        });

        $(".modal").on('hidden.bs.modal', function (e) {
            $(this).find('iframe').attr('src', '');   
        });
    }
}

BINARIUM.elements.ScrollReveal = {
    init: function () {
        window.sr = new scrollReveal();
    }
}


BINARIUM.elements.Tooltip = {
    init: function () {
        $('[data-toggle="tooltip"]').tooltip() 
    }
}



BINARIUM.elements.Tabs = {
    init: function () {
        $('.js-tabs').each(function(){
          $(this).find('[data-toggle="tab"]').first().tab('show')
        });  

        $('.tabs li').on('click', function (e) {
            $(this).find('[data-toggle="tab"]').tab('show');
        })
    }
}

BINARIUM.elements.Dropdown = {
    init: function () {
        $('.js-select').selectric();
    }
}

BINARIUM.elements.GMTSession = {

    gmt: 0, 

    init: function () {

        var self = this;
        
        $('.js-gmt-sessions').on('change', function () {
            var gmt = $('option:selected', this).data('gmt');
            self.gmt = self.getTime(gmt);
            self.setGMT();
        });
    },

    setGMT: function () {
        var gmt,
            self = this;
        
        $("[data-time]").each(function () {
            var time,
                newTime,
                _this = $(this);

            value = _this.data('time').split('–');

            newTime = '';
            
            for(var i = 0; i < value.length; i++) {
                time = self.getTime(value[i]);

                if (i == 1) {
                    newTime += '-';
                }

                newTime += self.setTime(time[0], time[1]);
            }

            _this.text(newTime);
 
        });
    },

    setTime: function (h, m) {
        var hr, min;

        hr = parseInt(h) + parseInt(this.gmt[0]);
        if (hr >= 24) {
            hr -= 24;
        }

        if (hr < 0) {
            hr += 24;
        }

        min = m;
        
        if (typeof(this.gmt[1]) !== 'undefined') {
            min = parseInt(m) + parseInt(this.gmt[1]);
        }

        if (hr.toString().length == 1) {
            hr = '0' + hr;
        }
        

        return hr + ':' + min;
        
    },

    getTime: function (val) {
        return val.toString().split(':')
    }
}

BINARIUM.start();