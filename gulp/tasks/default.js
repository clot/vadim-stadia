'use strict';

var gulp = require('gulp');

gulp.task('default', ['clean'], function() {
    gulp.start('copy-index', 'stylus', 'markup', 'images', 'sprites','scripts', 'watch');
});