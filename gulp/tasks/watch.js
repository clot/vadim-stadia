"use strict";

var gulp     = require('gulp'),
    config   = require('../config'),
    finder   = require('../helpers/finder');

gulp.task('watch', ['sync'], function() {
    gulp.watch(config.copy_index.file, ['copy-index']);
    gulp.watch(config.stylus.fd, ['stylus']);
    gulp.watch(config.images.src, ['images']);
    gulp.watch(config.js.dir, ['scripts']);
    gulp.watch(config.sprites.src + '*.*', ['sprites']);
    gulp.watch(finder(config.markup.src, 'jade'), ['markup']);
});
