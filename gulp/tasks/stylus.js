'use strict';

var gulp         = require('gulp'),
    stylus       = require('gulp-stylus'),
    sourcemaps   = require('gulp-sourcemaps'),
    plumber      = require('gulp-plumber'),
    errHandler   = require('../util/handleErrors'),
    autoprefixer = require('autoprefixer-stylus'),
    sync         = require('browser-sync'),
    config       = require('../config').stylus;

gulp.task('stylus', function () {
    
    var minify = global.env == 'PROD' ? true : false;
    var ap = global.env == 'PROD' ? [autoprefixer({browsers: ['last 3 versions']})] : [];
    
    return gulp.src(config.src)
            .pipe(plumber({errorHandler: errHandler}))
            .pipe(stylus( {compress: minify, url: 'embedurl', use: ap } ))
            .pipe(gulp.dest(config.dest))
            .pipe(sync.reload({stream:true}));
});    