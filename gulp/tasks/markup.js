'use strict';

var gulp        = require('gulp'),
    jade        = require('gulp-jade'),
    jadeInherit = require('gulp-jade-inheritance'),
    changed     = require('gulp-changed'),
    cached      = require('gulp-cached'),
    filter      = require('gulp-filter'),
    plumber     = require('gulp-plumber'),
    sync        = require('browser-sync'),
    config      = require('../config').markup,
    errHandler  = require('../util/handleErrors'),
    finder      = require('../helpers/finder');

gulp.task('markup', function () {
    return gulp.src(finder(config.src,'jade'))
            .pipe(plumber({errorHandler: errHandler}))
            .pipe(changed(config.dest, {extension: '.html'}))
            .pipe(cached('jade'))
            .pipe(jadeInherit({basedir: config.src}))
            .pipe(filter(function (file) {
                return !/\/_/.test(file.path) && !/^_/.test(file.relative);
            }))
            .pipe(jade(config.params))
            .pipe(gulp.dest(config.dest))
            .pipe(sync.reload({stream:true}))
});
