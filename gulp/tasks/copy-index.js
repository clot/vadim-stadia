'use strict';

var gulp   = require('gulp'),
    config = require('../config').copy_index;

gulp.task('copy-index', function() {
    return gulp.src(config.file)
            .pipe(gulp.dest(config.dest));
});