'use strict';

var sync    = require('browser-sync'),
    gulp    = require('gulp'),
    config  = require('../config').sync;

gulp.task('sync', function() {
    sync(config);
});
