'use strict';

var gulp        = require('gulp'),
    config      = require('../config').sprites,
    spritesmith = require('gulp.spritesmith');

gulp.task('sprites', function() {
    var spriteData = 
        gulp.src(config.src + '*.*') 
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.styl',
                cssFormat: 'stylus',
                algorithm: 'binary-tree',
                cssTemplate: config.src + 'tpl/stylus.template.mustache',
                cssVarMap: function(sprite) {
                    sprite.name = 's-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest(config.dest)); 
    spriteData.css.pipe(gulp.dest(config.styles + 'sprites/')); 
});