'use strict';

var gulp    = require('gulp'),
    changed = require('gulp-changed'),
    cached  = require('gulp-cached'),
    config  = require('../config').images;

gulp.task('images', function() {
    return gulp.src(config.src)
            .pipe(changed(config.dest))
            .pipe(cached('images'))
            .pipe(gulp.dest(config.dest));
});