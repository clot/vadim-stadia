'use strict';

var gulp = require('gulp');

gulp.task('build', ['clean'], function() {
    global.env = 'PROD';
    gulp.start('stylus', 'markup', 'images', 'sprites', 'scripts');
});