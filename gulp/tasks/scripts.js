'use strict';

var gulp         = require('gulp'),
    config       = require('../config').js,
    uglify       = require('gulp-uglify'),
    sync         = require('browser-sync'),
    concat       = require('gulp-concat'),
    gulpif       = require('gulp-if');

gulp.task('scripts', function() {

    var minify = global.env == 'PROD' ? true : false;

    return gulp.src(config.src)
                .pipe(concat(config.file))
                .pipe(gulp.dest(config.dest))
                .pipe(sync.reload({stream:true}));

});