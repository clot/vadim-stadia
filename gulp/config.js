var 
    _dest = './build/',
    _src  = 'src/';

var 
    _js  = 'js/',
    _css = 'css/';

var 
    _fileJs     = 'app.js';

module.exports = {
    
    sync: {
        server: {
            baseDir: _dest
        }
    },

    copy_index: {
        file: _src + '/index.html',
        dest: _dest
    },

    stylus: {
        fd: _src + _css + '**/*.styl',
        src: _src + _css + 'app.styl',
        dest: _dest + _css,
    },

    js: {
        dir: _src + _js + '**/*.js',
        src: [
            _src + _js + 'libs/jquery.js',
            _src + _js + 'libs/transition.js',
            _src + _js + 'libs/collapse.js',
            _src + _js + 'libs/tab.js',
            _src + _js + 'libs/modal.js',
            _src + _js + 'libs/tooltip.js',
            _src + _js + 'libs/selectric.js',
            _src + _js + 'libs/scrollreveal.js',
            _src + _js + 'app.js'
        ],
        file: _fileJs,
        dest: _dest + _js
    },

    sprites: {
        src: _src + 'sprites/',
        dest: _dest + 'images/',
        styles: _src + _css
    },

    images: {
        src: _src + 'images/*.*',
        dest: _dest + 'images/'
    },

    markup: {
        src: _src + 'jade/',
        dest: _dest,
        params: {                       
            pretty: true
        }
    },

    production: {
        cssSrc: _dest + _css + '*.css',
        jsSrc: _dest + _js + _fileJs,
        dest: _dest + 'compiled'
    },

    clean: {
        dest: _dest
    }
};